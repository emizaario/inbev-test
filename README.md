# Inbev Test


Projects with a service to find large cities in United States and Canada by name o some text sending as param.

## Features

- Service to find cities with a search word.
- We get the information from [geonames].
- Api deployed on AWS Lambda functions.


## Tech

This test uses a number of open source projects to work properly:

- [Python] - one of most popular programming language.
- [Flask] - Super small Framework to create web apps.
- [Flask RESTful] - Extension for [Flask] for build REST APIs.
- [Zappa] - Makes easy build and deploy server-less.


## Installation

You need [Python] v3+ to run.

In root of project we need to create a [virtual environment] and activate.

```sh
python3 -m venv venv
source venv/bin/activate
```

Install dependencies, all dependencies are located in ``requirements.txt``.

```sh
pip install -r requirements.txt
```

## Development

If you already installed all packages, you can run the app just typing:

```sh
python run.py
```

This command will run the app on localhost environment.

#### Run tests

I added some test to check the right responses on search city service.
To run the tests you only have to execute this command:

```sh
py.test
```

#### Configure Zappa
You neet to create AWS account if you don't have one, configure your credentials.
Run this command to set your credentials.

**NOTE: you need to have [aws-cli] installed before run this command.
If already have configurated your aws credentials just skip this step.**

```sh
aws configure
```

Your configuration will be saved in this path: ``~/.aws/config``

To communicate with AWS environment and deploy our app as lambda function you need to configure before [Zappa]:

```sh
zappa init
```
This command will ask for a couple basic settings, just follow the instructions of command line.



For deploy first time:

```sh
zappa deploy
```
**This command will be return a link of your app is running.**

For any update that we need to deploy on aws:

```sh
zappa update
```
**This command will be return a link of your app is running.**

#### Configure API key
If we need to configure an api client key we need to follow the doc of aws: https://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-setup-api-key-with-console.html

This step needs a extra config in your ``zappa_settings.json``:

```json
  "api_key_required": true
```

You must to send an extra header in all request **x-api-key** with api key showed in Api Gateway configuration




**Hope your like!**

best regards
**Pedro Ramirez**

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)


   [Python]: <https://docs.python.org/3/>
   [Flask]: <https://flask.palletsprojects.com/en/2.0.x/>
   [Flask RESTful]:<https://flask-restful.readthedocs.io/en/latest/>
   [Zappa]:<https://github.com/zappa/Zappa>
   [virtual environment]: <https://docs.python.org/3/tutorial/venv.html>
   [aws-cli]: <https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-welcome.html>
   [geonames]: <http://www.geonames.org/>
