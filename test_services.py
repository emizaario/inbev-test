import unittest
from app.load import create_app


class TestWelcome(unittest.TestCase):
    def setUp(self):
        self.app = create_app().test_client(self)

    def test_testService(self):
        """ Test to check if test service Works Fine"""
        response = self.app.get('/api/v1/test')

        assert response.status_code == 200

    def test_search_a_city_successfully(self):
        """ Test to call the api search and get 200 response """
        search_w = 'London'
        response = self.app.get(f'/api/v1/search?q={search_w}')

        assert response.status_code == 200


    def test_search_a_city_without_params(self):
        """ If we call search api without 'q' param
            we should get a bad request (400)"""
        response = self.app.get('/api/v1/search')

        assert response.status_code == 400
