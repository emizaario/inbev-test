from flask import request, app, Blueprint
from flask_restful import Resource, Api

from app.controllers.test import TestController
from app.controllers.search_cities import SearchController

api_bp = Blueprint('api', __name__)
api = Api(api_bp, prefix="/api/v1")

api.add_resource(TestController, '/test')
api.add_resource(SearchController, '/search')
