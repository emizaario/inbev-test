from flask import request
from flask_restful import Resource
from app.services.geocoder_service import GeocoderService
from app.exceptions.geocoder_exception import GeocoderCustomException

TOLERANCE = 5

class SearchController(Resource):
    """
    Class geocode search autocomplete
    """

    def get(self):
        try:
            search_word = request.args.get('q', None, type=str)

            if search_word is None:
                return {'message': "'q' param cant be null or empty"}, 400

            latitude = request.args.get('latitude', None, type=float)
            longitude = request.args.get('longitude', None, type=float)

            bounds = None

            if latitude != None and longitude != None:
                bounds = {
                'southwest': [(latitude - TOLERANCE), (longitude - TOLERANCE)],
                'northeast': [(latitude + TOLERANCE), (longitude + TOLERANCE)]
                }

            response = GeocoderService().search_large_city(search_word, bounds)

            return {'search': response}
        except GeocoderCustomException as ex:
            return {'search': []}
