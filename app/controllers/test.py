from flask import request
from flask_restful import Resource

class TestController(Resource):
    """
     Just to test API
    """

    def get(self):
        return  {'status': 'ok'}
