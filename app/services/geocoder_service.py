import geocoder
from app.exceptions.geocoder_exception import GeocoderCustomException

# TODO: Move those constants to session variable
COUNTRIES = ['US', 'CA']
CITIES = 'cities15000'
MAX_ROWS = 20
USERNAME = 'emizaario'
FUZZY='0.3'

class GeocoderService():
    """ Service to consume geonames services API """

    def search_large_city(self, serach_word, bounds=None):
        """This function only get large cities of CA AND US"""
        result_geocoder = []
        response = []
        if bounds != None:
            result_geocoder = geocoder.geonames(serach_word,
                                                maxRows=MAX_ROWS,
                                                country=COUNTRIES,
                                                key=USERNAME,
                                                cities=CITIES,
                                                proximity=bounds,
                                                fuzzy=FUZZY)
        else:
            result_geocoder = geocoder.geonames(serach_word,
                                                maxRows=MAX_ROWS,
                                                country=COUNTRIES,
                                                key=USERNAME,
                                                cities=CITIES,
                                                fuzzy=FUZZY)
        if result_geocoder.ok == False:
            # print the error to can be able to see on CloudWatch
            print(result_geocoder.error)
            raise GeocoderCustomException()

        for item in result_geocoder:
            name = (item.address + ', '
                    + item.raw["adminCodes1"]["ISO3166_2"] + ', '
                    + item.country)
            response.append({
                'name': name,
                'latitude': item.lat,
                'longitude': item.lng,
                # TODO: ask to the team how can i get this value
                'score':item.confidence
                })

        return response
